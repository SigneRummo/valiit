public class Day02 {
    public static void main(String[] args) {
        byte b1 = 65; // Suurus: 1 bait väike arv
        char c1 = 'B'; // 1 bait, üks tähemärk
        short s1 = 4578; // 2 baiti, keskmise suurusega arv
        int i1 = 56_343_343; // 4 baiti, üle 2 miljardi
        long l1 = 10_000_000_000_000L; // 8 baiti, väga suured arvud
        boolean isItSpring = false; // 1 bait, kas tõene või väär
        float fi = 45.684f; //  4 baiti. Võrdlemisi suur, aga ebatäpne
        double d1 = 567.38927; // 8 baiti. Veelgi suurem komakohaga arv, aga ikkagi ebatäpne

        /*System.out.println((char) b1);
        System.out.println(c1);*/

        int i3 = (int)l1; //Võib minna andmeid kaduma, sest surutakse jõuga suur arv väiksemaks
        int i4 = 5;
        int i5 = 6;
        int i6 = 45;
//        System.out.println(i4 == i6); // sama oleks, kui defineeriks booleani
        boolean test1 = i4 == i6;
//        System.out.println(test1);
        //++
        int i7 = 50;
/*
        System.out.println(i7++); // oluline, kas plussid on ees või järel, preagu prindib ikkagi 50
*/
        //-=
        int i9 = 11;
        i9 = i9 + 5;
        i9 += 5; //sama mis eelmine
        // || või operaator
        // && kõik on võrdsed, siis on true
        // mooduliga ehk jäägiga jagamine %
        int i14 = 11;
        int i15 = 10;
        /*System.out.println(i14 % 10); // vastus 1 (jääk 1)
        System.out.println(i15 % 10); // vastus 0 (jääk 0)
*/
        // Wrapper klassid
        /*Integer i20 = 20;
        Integer i21 = new  Integer ( 20); //need on sama sisuga read
        int a = 1;
        int b = 1;
        int c = 3;*/
        /*System.out.println(a == b);
        System.out.println(a == c);*/
        /*a = c;*/
        /*System.out.println(a == b);
        System.out.println(a == c);*/
        int x1 = 10;
        int x2 = 20;
        int y1 = ++x1;
        /*System.out.println("x1: " + x1);
        System.out.println("y1: " + y1);
        int y2 = x2++;
        System.out.println("x2: " + x2);
        System.out.println("y2: " + y2);*/
        /*int a = 18 % 3;
        int b = 19 % 3;
        int c = 20 % 3;
        int d = 21 % 3;
        System.out.println(a);
        System.out.println(b);
        System.out.println(c);
        System.out.println(d);*/
        /*System.out.println("Isa ütles: \"Tule siia!\"");

        String myText1 = "This is my text.";
        String myText2 = "This is another text.";
        myText1 = myText1 + myText2;
        System.out.println(myText1);
        String myText3 = "T3";
        String myText4 = "T3";
        System.out.println(myText3 == myText4); // ei ole korrektne, sest ei võrdle teksti, vaid mälupiirkonna aadressi
        System.out.println(myText3.equals(myText4)); // see on korrektne, võrdleb teksti
        System.out.println(myText3.equalsIgnoreCase("t3")); // ei pane suurtähte tähele
        String myText5 = new String("Some text..."); // see on korrektne viis objekti luua, aga stringi puhul ülearune*/

        System.out.println("Hello, World!");
        System.out.println("Hello, \"World!\"");
        System.out.println("Steven Hawking once said: \"Life would be tragic if it weren’t funny\".");
        String tekst1 = "\"See on teksti esimene pool\"";
        String tekst2 = "\"See on teksti teine pool\"";
        System.out.println("Kui liita kokku sõned " + tekst1 + " ja " + tekst2 + ", siis tulemuseks saame \n\"See on teksti esimene pool See on teksti teine pool\".");
        System.out.println("Elu on ilus.");
        System.out.println("Elu on \'ilus\'.");
        System.out.println("Elu on \"ilus?\".");
        System.out.println("Kõige rohkem segadust tekitab \"-märgi kasutamine sõne sees.");
        System.out.println("Eesti keele kõige ilusam lause on: \"Sõida tasa üle silla!\"");
        System.out.println("\'Kolm\' - kolm, \'neli\' - neli, \"viis\" - viis.");
        String tallinnPopulation​ = "450 000";
        System.out.println("Tallinnas elab " + tallinnPopulation​ + " inimest.");
        int populationOfTallinn​ = 450_000;
        System.out.println("Tallinnas elab " + populationOfTallinn​ + " inimest.");
        System.out.println(
                String.format("Tallinnas elab %,d inimest", populationOfTallinn​));
        String planet1 = "Merkuur";
        String planet2 = "Veenus";
        String planet3 = "Maa";
        String planet4 = "Marss";
        String planet5 = "Jupiter";
        String planet6 = "Saturn";
        String planet7 = "Uraan";
        String planet8 = "Neptuun";
        int planetCount​ = 8;
        System.out.println(
                String.format("%s, %s, %s, %s, %s, %s, %s, %s on Päikesesüsteemi %d planeeti.",
                        planet1, planet2, planet3, planet4, planet5, planet6, planet7, planet8, planetCount​));
        String bookTitle = "\"Rehepapp\"";
        System.out.println("Raamatu " + bookTitle + " autor on Andrus Kivirähk." );




    }
}
